export interface Exercise {
    id: number;
    exercise_name: string;
    sets: number;
    reps: number;
    rest_time: object;
    workout_id: number;
}