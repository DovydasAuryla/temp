import { Component, ViewChild } from '@angular/core';

import { Platform, ModalController, AlertController, IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PlatformLocation } from '@angular/common';
import { async } from '@angular/core/testing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild(IonRouterOutlet, { static: false }) routerOutlet: IonRouterOutlet;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private localtion: PlatformLocation,
    private modalController: ModalController,
    private alertController: AlertController,
    private router: Router
  ) {
    this.initializeApp();

    this.localtion.onPopState(async () => {
      console.log('ON POP');
      const modal = await this.modalController.getTop();
      if (modal) {
        modal.dismiss();
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.platform.backButton.subscribeWithPriority(0, async () => {
        if (this.routerOutlet && this.routerOutlet.canGoBack()) {
          this.routerOutlet.pop();
        } else if (this.router.url === '/tabs/workouts') {
          const alert = await this.alertController.create({
            header: 'Close App',
            message: 'Do you really want to close the app?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Close App',
                handler: () => {
                  navigator["app"].exitApp();
                }
              }
            ]
          });
        }
      });
    });
  }
}
