import { Component, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.component.html',
  styleUrls: ['./modal-page.component.scss'],
})
export class ModalPageComponent {

  // Data passed in by componentProps
  @Input() exerciseName: string;
  @Input() sets: string;
  @Input() reps: string;
  @Input() rest: string;

  constructor(navParams: NavParams, public modalController: ModalController) {}

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss();
  }

  saveEdits() {
    this.modalController.dismiss({
      exerciseName: this.exerciseName,
      sets: this.sets,
      reps: this.reps,
      rest: this.rest
    });
  }
}
