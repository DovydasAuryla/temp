import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'workouts',
        children: [
          {
            path: '',
            loadChildren: () => import('../workouts/workouts.module').then(m => m.WorkoutsPageModule)
          },
          {
            path: ':id',
            loadChildren: () => import('../exercises/exercises.module').then(m => m.ExercisesPageModule)
          },
          {
            path: 'new-exercise/:id',
            loadChildren: () => import('../new-exercise/new-exercise.module').then(m => m.NewExercisePageModule)
          }
        ]
      },
      {
        path: 'timer',
        children: [
          {
            path: '',
            loadChildren: () => import('../timer/timer.module').then(m => m.TimerPageModule)
          }
        ]
      }//,
      // {
      //   path: 'new-exercise',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: () => import('../new-exercise/new-exercise.module').then( m => m.NewExercisePageModule)
      //     }
      //   ]
      // }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/workouts',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRouterModule { }