import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Exercise } from 'src/app/models/exercise';
import { ExerciseService } from 'src/app/services/exercise.service';
import { Workout } from 'src/app/models/workout';
import { WorkoutService } from 'src/app/services/workout.service';

@Component({
  selector: 'app-new-exercise',
  templateUrl: './new-exercise.page.html',
  styleUrls: ['./new-exercise.page.scss'],
})
export class NewExercisePage implements OnInit {

  workoutId = null;
  fullTime: any = '00:00:00';
  // minutes: number = null;
  // seconds: number = null;

  exerciseName: string = null;
  sets: number = null;
  reps: number = null;

  workout: Workout = {} as Workout;

  // tslint:disable-next-line: max-line-length
  constructor(private workoutService: WorkoutService, private activatedRoute: ActivatedRoute, private router: Router, private exerciseService: ExerciseService) { }

  ngOnInit() {
    this.workoutId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewWillEnter() {
    this.workoutId = this.activatedRoute.snapshot.paramMap.get('id');

    this.workoutService.getWorkoutById(this.workoutId).then(res => {
      this.workout = res;
    });
  }

  discrease(value) {
    if ((value === 'sets') && (this.sets > 0)) {
      this.sets--;
    } else if ((value === 'reps') && (this.reps > 0)) {
      this.reps--;
    }
  }

  increase(value) {
    if (value === 'sets') {
      this.sets++;
    } else {
      this.reps++;
    }
  }

  saveExercise() {
    const exercise: Exercise = {} as Exercise;

    if (this.exerciseName && this.reps && this.sets) {
      exercise.exercise_name = this.exerciseName;
      exercise.reps = this.reps;
      exercise.sets = this.sets;
      exercise.rest_time = this.fullTime;
      exercise.workout_id = this.workoutId;
      this.exerciseService.addExercise(exercise).then(() => {
        this.router.navigateByUrl(`/tabs/workouts/${this.workoutId}`);
        this.fullTime = '00:00:00';
        this.exerciseName = null;
        this.sets = null;
        this.reps = null;
      });
    } else {
      alert('Please fill all required fields.');      // Need to add native alert function;
    }

    // const timeSplit = this.fullTime.split(':');
    // this.minutes = timeSplit[1];
    // this.seconds = timeSplit[2];
  }

}
