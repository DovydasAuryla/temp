import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WorkoutService } from 'src/app/services/workout.service';
import { Observable } from 'rxjs';
import { Workout } from 'src/app/models/workout';
import { WorkoutsPageRoutingModule } from './workouts-routing.module';
import { AlertController, Platform } from '@ionic/angular';
import { ExerciseService } from 'src/app/services/exercise.service';


@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.page.html',
  styleUrls: ['./workouts.page.scss'],
})
export class WorkoutsPage implements OnInit {

  // workouts: Workout[] = [];

  workouts: Observable<any>;

  newWorkout: Workout = {} as Workout;
  dialogResult: any;

  // tslint:disable-next-line: max-line-length
  constructor(public alertController: AlertController, private router: Router, private workoutService: WorkoutService, private exerciseService: ExerciseService) { }

  ngOnInit() {
    this.workoutService.getAllWorkouts().then(result => {
      this.workouts = result;
    });
  }

  async presentAlert() {
    let okClicked = false;
    const alert = await this.alertController.create({
      header: 'Workout',
      message: 'In this created workout you will be able to add exercises',
      inputs: [
        {
          name: 'workout_name',
          type: 'text',
          placeholder: 'Enter your workout name',
          id: 'workout_name_input'
          // value: 'hello',
          // type: 'url',
          // type: 'date',
          // min: '2017-03-01',
          // max: '2018-01-12'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            okClicked = false;
          }
        }, {
          text: 'Ok',
          handler: () => {
            okClicked = true;
          }
        }
      ]
    });
    await alert.present();
    this.dialogResult = await alert.onDidDismiss();

    if (okClicked) {
      this.saveWorkout();
    }
  }

  saveWorkout() {
    this.newWorkout.workout_name = this.dialogResult.data.values.workout_name;
    this.workoutService.addWorkout(this.newWorkout).then(() => {
      this.workoutService.getAllWorkouts().then(result => {
        this.workouts = result;
      });
    });
  }

  onWorkoutCard(workout) {
    this.exerciseService.getExercisesByWorkoutId(workout).then(res => {
      if (res.length === 0) {
        this.router.navigateByUrl(`/tabs/workouts/new-exercise/${workout}`);
      } else {
        this.router.navigateByUrl(`/tabs/workouts/${workout}`);
      }
    });
  }














  openExercises() {
    this.router.navigateByUrl(`/tabs/workouts/42`);
  }

}
