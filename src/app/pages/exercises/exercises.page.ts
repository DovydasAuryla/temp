import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Workout } from 'src/app/models/workout';
import { ExerciseService } from 'src/app/services/exercise.service';
import { Exercise } from 'src/app/models/exercise';
import { WorkoutService } from 'src/app/services/workout.service';
import { AlertController } from '@ionic/angular';

import { ModalController } from '@ionic/angular';
import { ModalPageComponent } from 'src/app/modals/modal-page/modal-page.component';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.page.html',
  styleUrls: ['./exercises.page.scss'],
})
export class ExercisesPage {

  workoutId: any;
  exercises: Exercise[] = [];
  workout: Workout = {} as Workout;
  editedExercise: Exercise = {} as Exercise;

  dialogResult: any;

  // tslint:disable-next-line: max-line-length
  constructor(public modalController: ModalController, public alertController: AlertController, private activatedRoute: ActivatedRoute, private router: Router, private exerciseService: ExerciseService, private workoutService: WorkoutService) {
  }

  ionViewWillEnter() {
    this.workoutId = this.activatedRoute.snapshot.paramMap.get('id');

    this.workoutService.getWorkoutById(this.workoutId).then(res => {
      this.workout = res;      
    });

    this.exerciseService.getExercisesByWorkoutId(this.workoutId).then(res => {
      this.exercises = res;
    });
  }

  onFabClicked() {
    this.router.navigateByUrl(`/tabs/workouts/new-exercise/${this.workoutId}`);
  }

  async presentModal(exercise) {
    this.editedExercise = exercise;
    const modal = await this.modalController.create({
      component: ModalPageComponent,
      componentProps: {
        exerciseName: exercise.exercise_name,
        sets: exercise.sets,
        reps: exercise.reps,
        rest: exercise.rest_time
      }
    });

    modal.present();
    const { data } = await modal.onWillDismiss();

    if (data) {
      this.editedExercise.sets = data.sets,
        this.editedExercise.reps = data.reps,
        this.editedExercise.rest_time = data.rest;

      this.exerciseService.updateExercise(this.editedExercise).then(res => {
      });
    }

    // For close modal, when browser "back" button pressed
    if (!window.history.state.modal) {
      const modalState = {modal: true};
      history.pushState(modalState, null);
    }
  }

  delete(id) {
    this.exerciseService.deleteExercise(id).then(res => {
      this.exercises = [];
      res.forEach(element => {
        if (element.workout_id === this.workoutId) {
          this.exercises.push(element);
        }
      });
    });
  }

}
