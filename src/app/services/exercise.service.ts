import { Injectable } from '@angular/core';
import { Workout } from '../models/workout';
import { Exercise } from '../models/exercise';
import { Storage } from '@ionic/storage';

const STORAGE_KEY = 'exercises';

@Injectable({
  providedIn: 'root'
})
export class ExerciseService {

  constructor(private storage: Storage) { }

  getAllExercises() {
    return this.storage.get(STORAGE_KEY);
  }

  getExercisesByWorkoutId(workoutId) {
    const exercisesByWorkout: Exercise[] = [];
    return this.storage.get(STORAGE_KEY).then(res => {
      if (res !== null) {
        res.forEach(element => {
          if (Number(element.workout_id) === Number(workoutId)) {
            exercisesByWorkout.push(element);
          }
        });
      }
      return exercisesByWorkout;
    });
  }

  addExercise(exercise: Exercise) {
    exercise.id = Date.now();
    return this.getAllExercises().then(result => {
      if (result) {
        result.push(exercise);
        return this.storage.set(STORAGE_KEY, result);
      } else {
        return this.storage.set(STORAGE_KEY, [exercise]);
      }
    });
  }

  updateExercise(item: Exercise): Promise<any> {
    return this.storage.get(STORAGE_KEY).then((items: Exercise[]) => {
      if (!items || items.length === 0) {
        return null;
      }
      const newItems: Exercise[] = [];
      for (const i of items) {
        if (i.id === item.id) {
          newItems.push(item);
        } else {
          newItems.push(i);
        }
      }
      return this.storage.set(STORAGE_KEY, newItems);
    });
  }















  isExercise(exercisesId) {
    return this.getAllExercises().then(result => {
      return result && result.indexOf(exercisesId) !== -1;
    });
  }

  deleteExercise(exercisesId) {
    console.log(exercisesId);

    return this.getAllExercises().then(result => {
      if (result) {
        const index = result.findIndex(x => x.id === exercisesId);
        result.splice(index, 1);
        return this.storage.set(STORAGE_KEY, result);
      }
    });
  }
}
