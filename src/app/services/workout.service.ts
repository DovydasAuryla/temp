import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Workout } from '../models/workout';
import { Observable } from 'rxjs';

const STORAGE_KEY = 'workouts';

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  constructor(private storage: Storage) { }

  getAllWorkouts() {
    return this.storage.get(STORAGE_KEY);
  }

  addWorkout(workout: Workout) {
    workout.id = Date.now();
    return this.getAllWorkouts().then((result: Workout[]) => {
      if (result) {
        result.push(workout);
        return this.storage.set(STORAGE_KEY, result);
      } else {
        return this.storage.set(STORAGE_KEY, [workout]);
      }
    });
  }

  getWorkoutById(workoutId) {
    let workout: Workout = {} as Workout;
    return this.getAllWorkouts().then(res => {
      if (res) {
        res.forEach(element => {
          if (Number(element.id) === Number(workoutId)) {
            workout = element;
          }
        });
      }
      return workout;
    });
  }


















  // getWorkoutById(workoutId) {
  //   let workout;
  //   return this.getAllWorkouts().then(result => {
  //     if (result) {
  //       result.forEach(element => {
  //         if (Number(element.id) === Number(workoutId)) {
  //           workout = element;
  //           return workout;
  //         }
  //       });
  //     }
  //   });
  // }

  isWorkout(workoutId) {
    return this.getAllWorkouts().then(result => {
      return result && result.indexOf(workoutId) !== -1;
    });
  }


  deleteWorkout(workoutId) {
    return this.getAllWorkouts().then(result => {
      if (result) {
        const index = result.indexOf(workoutId);
        result.splice(index, 1);
        return this.storage.set(STORAGE_KEY, result);
      }
    });
  }

}
